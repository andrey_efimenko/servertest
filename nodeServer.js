var http = require('http');
var url = '127.0.0.1';
http.createServer(function(req, res) {

	res.writeHead(200, {
		"Content-Type" : "application/json"
	});
	
	
	var otherArray = ["item1", "item2"];
	var otherObject = {
		item1 : "Andrew Efimenko",
		item2 : "item2val"
	};
	var json = JSON.stringify({
		anObject : otherObject,
		anArray : otherArray,
		another : "item"
	});
	
	
	res.end(json);
}).listen(80, url);
console.log('Server running at http://' + url + ':80/'); 